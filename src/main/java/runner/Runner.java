package runner;

import inputDevices.devices.BarCodesScanner;
import outputDevices.devices.LcdDisplay;
import outputDevices.devices.Printer;
import products.ProductsRepository;
import products.model.Product;
import receipt.ReceiptService;
import receipt.model.Receipt;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class Runner {
    private static final String EXIT = "exit";
    private static final String START_TEXT = "\nPress enter to scan next product or type 'exit' to print receipt";
    private static final String INVALID_BARCODE = "Invalid bar-code";
    private static final Printer printer = new Printer();
    private static final LcdDisplay lcdDisplay = new LcdDisplay();
    private static final BarCodesScanner barCodesScanner = new BarCodesScanner();

    private static final ProductsRepository productsRepository = new ProductsRepository();
    private static final ReceiptService receiptService = new ReceiptService();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        List<Product> products = new ArrayList<Product>();
        while (true) {
            System.out.println(START_TEXT);
            String option = in.nextLine();

            if (option.toUpperCase().equals(EXIT.toUpperCase())) {
                Receipt receipt = receiptService.createReceipt(products);
                printer.printOnReceipt(receipt);
                lcdDisplay.printSum(receipt.getPrice());
                break;
            } else {
                Integer barCode = barCodesScanner.getNextBarCode();
                if (barCode == null) {
                    lcdDisplay.printError(INVALID_BARCODE);
                    continue;
                }
                Product product = productsRepository.findByBarCode(barCode);
                if (product != null) {
                    products.add(product);
                }
                lcdDisplay.printOnScreen(product);
            }
        }
    }
}
