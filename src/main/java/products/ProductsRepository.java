package products;

import database.DatabaseClient;
import products.model.Product;

import java.util.List;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class ProductsRepository {

    private final DatabaseClient databaseClient = new DatabaseClient();

    public DatabaseClient getDatabaseClient() {
        return databaseClient;
    }

    public Product findByName(String name) {
        List<Product> allProducts = databaseClient.getAllProducts();
        for (Product product : allProducts) {
            if (product.getName().toUpperCase().equals(name.toUpperCase())) {
                return product;
            }
        }
        return null;
    }

    public Product findByBarCode(Integer barCode) {
        List<Product> allProducts = databaseClient.getAllProducts();
        for (Product product : allProducts) {
            if (product.getBarCode().equals(barCode)) {
                return product;
            }
        }
        return null;
    }


    public Product findById(Integer id) {
        List<Product> allProducts = databaseClient.getAllProducts();
        for (Product product : allProducts) {
            if (product.getId().equals(id)) {
                return product;
            }
        }
        return null;
    }
}
