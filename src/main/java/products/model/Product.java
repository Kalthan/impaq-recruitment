package products.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class Product {
    private Integer id;
    private Integer barCode;
    private String name;
    private BigDecimal price;

    public Product(Integer id, Integer barCode, String name, BigDecimal price) {
        this.id = id;
        this.barCode = barCode;
        this.name = name;
        this.price = price.setScale(2, RoundingMode.HALF_EVEN);
    }

    public Integer getBarCode() {
        return barCode;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

}
