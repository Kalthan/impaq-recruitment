package inputDevices;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public interface InputDevice {
    Integer getNextBarCode();
}
