package inputDevices.devices;

import inputDevices.InputDevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class BarCodesScanner implements InputDevice {
    private List<Integer> barCodesExample;
    private Random random = new Random();

    public BarCodesScanner() {
        barCodesExample = new ArrayList<Integer>();
        barCodesExample.add(1111);
        barCodesExample.add(2222);
        barCodesExample.add(3333);
        barCodesExample.add(4444);
        barCodesExample.add(5555);
        barCodesExample.add(6666);
        barCodesExample.add(null);
        barCodesExample.add(null);
    }

    public Integer getNextBarCode() {
        int exampleBarCodeId = random.nextInt(barCodesExample.size());
        return this.barCodesExample.get(exampleBarCodeId);
    }
}
