package receipt.model;

import products.model.Product;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class Receipt {
    private List<Product> products;
    private BigDecimal price;
    private Timestamp creationDate;

    public Receipt(List<Product> products, BigDecimal price, Timestamp creationDate) {
        this.products = products;
        this.price = price.setScale(2, RoundingMode.HALF_EVEN);
        this.creationDate = creationDate;
    }

    public List<Product> getProducts() {
        return products;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }
}
