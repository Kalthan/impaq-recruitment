package receipt;

import products.model.Product;
import receipt.model.Receipt;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class ReceiptService {

    public Receipt createReceipt(List<Product> products) {
        BigDecimal price = new BigDecimal(0);
        for (Product product : products) {
            price = price.add(product.getPrice());
        }
        return new Receipt(products, price, new Timestamp(System.currentTimeMillis()));
    }
}
