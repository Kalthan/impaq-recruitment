package database;

import products.model.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class DatabaseClient {
    private final String BATON = "baton";
    private final String CHLEB = "chleb";
    private final String KOTLET = "kotlet";
    private final String WODA = "woda";

    List<Product> products;

    public DatabaseClient() {
        products = new ArrayList<Product>();
        products.add(new Product(1, 1111, BATON, new BigDecimal(1.21)));
        products.add(new Product(2, 2222, CHLEB, new BigDecimal(3.35)));
        products.add(new Product(3, 3333, KOTLET, new BigDecimal(0.55)));
        products.add(new Product(4, 4444, WODA, new BigDecimal(15.12)));
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Product> getAllProducts() {
        return products;
    }

}
