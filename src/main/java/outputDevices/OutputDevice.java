package outputDevices;

import java.math.BigDecimal;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public interface OutputDevice {
    void printPrice(BigDecimal price);
    void printName(String productName);
}
