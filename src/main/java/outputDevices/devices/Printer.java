package outputDevices.devices;

import outputDevices.OutputDevice;
import products.model.Product;
import receipt.model.Receipt;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class Printer implements OutputDevice {
    private final String PRINTER = "Printer";
    private final String PRODUCT = "product: ";
    private final String PRICE = "price: ";
    private final String CURRENCY = "zł";
    private final String DATE = "date: ";
    private final String SUM = "sum: ";

    public void printPrinterHeader() {
        System.out.println(PRINTER);
    }

    public void printPrice(BigDecimal price) {
        System.out.println(PRICE + price.setScale(2, RoundingMode.HALF_EVEN) + CURRENCY);
    }

    public void printName(String productName) {
        System.out.println(PRODUCT + productName);
    }

    private void printBreakLine() {
        System.out.println("_______________________");
    }

    private void printReceiptCreationDate(Receipt receipt) {
        System.out.println("\n" + DATE + receipt.getCreationDate());
    }

    private void printSum(Receipt receipt) {
        System.out.println(SUM + receipt.getPrice().setScale(2, RoundingMode.HALF_EVEN) + CURRENCY);
    }

    public void printOnReceipt(Receipt receipt) {
        printPrinterHeader();
        for (Product product : receipt.getProducts()) {
            printName(product.getName());
            printPrice(product.getPrice());
            printBreakLine();
        }
        printSum(receipt);
        printReceiptCreationDate(receipt);
    }
}
