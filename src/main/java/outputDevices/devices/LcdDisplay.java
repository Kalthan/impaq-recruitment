package outputDevices.devices;

import outputDevices.OutputDevice;
import products.model.Product;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by m.radkiewicz on 2017-08-11.
 */
public class LcdDisplay implements OutputDevice {
    private final String LCD = "LCD";
    private final String PRODUCT_NOT_FOUND = "Product not found";
    private final String CURRENCY = "zł";
    private static final String SUM = "sum";


    public void printLcdHeader(){
        System.out.println(LCD);
    }

    public void printPrice(BigDecimal price) {
        System.out.println(price.setScale(2, RoundingMode.HALF_EVEN) + CURRENCY);
    }

    public void printName(String productName) {
        System.out.println(productName);
    }

    public void printError(String errorMessage){
        printLcdHeader();
        System.out.println(errorMessage);
    }
    public void printSum(BigDecimal sum){
        printLcdHeader();
        printName(SUM);
        printPrice(sum);
    }

    public void printOnScreen(Product product) {
        if (product != null) {
            printLcdHeader();
            printName(product.getName());
            printPrice(product.getPrice());
        } else {
            printError(PRODUCT_NOT_FOUND);
        }
    }
}
