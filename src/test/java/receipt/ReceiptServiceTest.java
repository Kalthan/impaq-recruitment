package receipt;

import org.junit.Test;
import products.model.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by m.radkiewicz on 2017-08-13.
 */
public class ReceiptServiceTest {
    @Test
    public void createReceiptTest() {
        ReceiptService receiptService = new ReceiptService();
        assertNotNull(receiptService.createReceipt(new ArrayList<Product>()));
    }

    @Test
    public void createReceiptPriceTest() {
        ReceiptService receiptService = new ReceiptService();

        List<Product> products = new ArrayList<Product>();
        products.add(new Product(1, 1, "testName1", new BigDecimal(1.20)));
        products.add(new Product(2, 2, "testName2", new BigDecimal(1.21)));
        BigDecimal price = new BigDecimal(0);
        for (Product product : products) {
            price = price.add(product.getPrice());
        }

        assertEquals(price, receiptService.createReceipt(products).getPrice());
    }

    @Test
    public void createReceiptProductsTest() {
        ReceiptService receiptService = new ReceiptService();

        List<Product> products = new ArrayList<Product>();
        products.add(new Product(1, 1, "testName1", new BigDecimal(1.20)));
        products.add(new Product(2, 2, "testName2", new BigDecimal(1.21)));

        assertEquals(products, receiptService.createReceipt(products).getProducts());
    }


    @Test
    public void createReceiptDateTest() {
        ReceiptService receiptService = new ReceiptService();

        List<Product> products = new ArrayList<Product>();
        products.add(new Product(1, 1, "testName1", new BigDecimal(1.20)));

        assertNotNull(receiptService.createReceipt(products).getCreationDate());
    }

}
