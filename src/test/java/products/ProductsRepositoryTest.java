package products;

import org.junit.Test;
import products.model.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by m.radkiewicz on 2017-08-13.
 */
public class ProductsRepositoryTest {

    @Test
    public void findByWrongNameTest() {
        ProductsRepository productsRepository = new ProductsRepository();

        List<Product> products = new ArrayList<Product>();
        products.add(new Product(1, 1, "testName", new BigDecimal(1.21)));
        productsRepository.getDatabaseClient().setProducts(products);

        assertNull(productsRepository.findByName("wrongTestName"));
    }

    @Test
    public void findByCorrectNameTest() {
        ProductsRepository productsRepository = new ProductsRepository();

        List<Product> products = new ArrayList<Product>();
        Product product = new Product(1, 1, "testName", new BigDecimal(1.21));
        products.add(product);
        productsRepository.getDatabaseClient().setProducts(products);

        assertEquals(product, productsRepository.findByName("testName"));
    }

    @Test
    public void findByWrongBarCode() {
        ProductsRepository productsRepository = new ProductsRepository();

        List<Product> products = new ArrayList<Product>();
        Product product = new Product(1, 1, "testName", new BigDecimal(1.21));
        products.add(product);
        productsRepository.getDatabaseClient().setProducts(products);

        assertNull(productsRepository.findByBarCode(2));
    }

    @Test
    public void findByCorrectBarCode() {
        ProductsRepository productsRepository = new ProductsRepository();

        List<Product> products = new ArrayList<Product>();
        Product product = new Product(1, 1, "testName", new BigDecimal(1.21));
        products.add(product);
        productsRepository.getDatabaseClient().setProducts(products);

        assertEquals(product, productsRepository.findByBarCode(1));
    }

    @Test
    public void findByWrongId() {
        ProductsRepository productsRepository = new ProductsRepository();

        List<Product> products = new ArrayList<Product>();
        Product product = new Product(1, 1, "testName", new BigDecimal(1.21));
        products.add(product);
        productsRepository.getDatabaseClient().setProducts(products);

        assertNull(productsRepository.findByBarCode(2));
    }

    @Test
    public void findByCorrectId() {
        ProductsRepository productsRepository = new ProductsRepository();

        List<Product> products = new ArrayList<Product>();
        Product product = new Product(1, 1, "testName", new BigDecimal(1.21));
        products.add(product);
        productsRepository.getDatabaseClient().setProducts(products);

        assertEquals(product, productsRepository.findById(1));
    }

    @Test
    public void getDatabaseClientTest() {
        ProductsRepository productsRepository = new ProductsRepository();
        assertNotNull(productsRepository.getDatabaseClient());
    }
}
